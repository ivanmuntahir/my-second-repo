<?php

	require_once('animal.php');
	require_once  ('ape.php');
	require_once  ('frog.php');

	$sheep = new Animal("shaun");
	echo $sheep->name; // "shaun"
	echo"<br>";
	echo $sheep->legs; // 2
	echo"<br>";
	echo $sheep->cold_blooded; // false
	echo"<br>";
	echo"<br>";

	$sungokong = new Ape("kera sakti");
	echo $sungokong->name; // "shaun"
	echo"<br>";
	echo $sungokong->legs; // 2
	echo"<br>";
	$sungokong->yell();
	echo"<br>";
	echo"<br>";

	$kodok = new Frog("buduk");
	echo $kodok->name;
	echo"<br>";
	echo $kodok->legs;
	echo"<br>";
	$kodok->jump();

?>